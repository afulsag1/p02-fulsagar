//
//  ViewTwo.m
//  exp
//
//  Created by Abhijit Fulsagar on 2/4/17.
//  Copyright © 2017 Abhijit Fulsagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import<UIkit/UIKit.h>

@interface ViewTwo :UIViewController
{
    IBOutlet UILabel *l00;
    IBOutlet UILabel *l01;
    IBOutlet UILabel *l02;
    IBOutlet UILabel *l03;
    IBOutlet UILabel *l10;
    IBOutlet UILabel *l11;
    IBOutlet UILabel *l12;
    IBOutlet UILabel *l13;
    IBOutlet UILabel *l20;
    IBOutlet UILabel *l21;
    IBOutlet UILabel *l22;
    IBOutlet UILabel *l23;
    IBOutlet UILabel *l30;
    IBOutlet UILabel *l31;
    IBOutlet UILabel *l32;
    IBOutlet UILabel *l33;
    IBOutlet UILabel *score;
    IBOutlet UILabel *highscore;
    IBOutlet UIButton *left;
    IBOutlet UIButton *right;
    IBOutlet UIButton *up;
    IBOutlet UIButton *down;
    int a,b,c,d,i,j,k,l,flag,score1,random,flag1,flag2,highscore1;
    int arr[4];
    NSString *result;
    NSString *zero;
    
}
@end

@implementation ViewTwo
-(void)savehighscore
{
    [highscore resignFirstResponder];
    NSString *value=highscore.text;
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"value"];
    [defaults synchronize];
    NSLog(@"data saved");
}

-(void)updatehighscore:(int)score1
{
    highscore1=[highscore.text integerValue];
    //NSLog(@"In high score function %@",score1);
    
    if(highscore1<score1)
    {
        highscore.text=[@(score1) stringValue];
    }
}
-(void)updaterow1
{
    a=[l00.text integerValue];
    b=[l01.text integerValue];
    c=[l02.text integerValue];
    d=[l03.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
}

-(void)updaterow2
{
    a=[l10.text integerValue];
    b=[l11.text integerValue];
    c=[l12.text integerValue];
    d=[l13.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
}


-(void)updaterow3
{
    a=[l20.text integerValue];
    b=[l21.text integerValue];
    c=[l22.text integerValue];
    d=[l23.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
  
}

-(void)updaterow4
{
    a=[l30.text integerValue];
    b=[l31.text integerValue];
    c=[l32.text integerValue];
    d=[l33.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
    
}

-(void)updatecolumn1
{
    a=[l00.text integerValue];
    b=[l10.text integerValue];
    c=[l20.text integerValue];
    d=[l30.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
}

-(void)updatecolumn2
{
    a=[l01.text integerValue];
    b=[l11.text integerValue];
    c=[l21.text integerValue];
    d=[l31.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
}
-(void)updatecolumn3
{
    a=[l02.text integerValue];
    b=[l12.text integerValue];
    c=[l22.text integerValue];
    d=[l32.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
}
-(void)updatecolumn4
{
    a=[l03.text integerValue];
    b=[l13.text integerValue];
    c=[l23.text integerValue];
    d=[l33.text integerValue];
    arr[0]=a;
    arr[1]=b;
    arr[2]=c;
    arr[3]=d;
}
-(void)randomnumbergenerater
{
    zero=@"0";
    flag1=0;
    while (flag1==0)
    {
        //random number generated
        random=arc4random_uniform(16);
        
        if(random==l00.tag)
        {
            if([l00.text isEqualToString:zero])
            {
                l00.text=@"2";
                flag1=1;
                [self updaterow1];
            }
        }
        else if(random==l01.tag)
        {
            if([l01.text isEqualToString:zero])
            {
                l01.text=@"2";
                flag1=1;
                [self updaterow1];
            }
        }
        else if(random==l02.tag)
        {
            if([l02.text isEqualToString:zero])
            {
                l02.text=@"2";
                flag1=1;
                [self updaterow1];
            }
        }
        else if(random==l03.tag)
        {
            if([l03.text isEqualToString:zero])
            {
                l03.text=@"2";
                flag1=1;
                 [self updaterow1];
            }
        }
        else if(random==l10.tag)
        {
            if([l10.text isEqualToString:zero])
            {
                l10.text=@"2";
                flag1=1;
                 [self updaterow2];
            }
        }
        else if(random==l11.tag)
        {
            if([l11.text isEqualToString:zero])
            {
                l11.text=@"2";
                flag1=1;
                 [self updaterow2];
            }
        }
        else if(random==l12.tag)
        {
            if([l12.text isEqualToString:zero])
            {
                l12.text=@"2";
                flag1=1;
                [self updaterow2];
            }
        }
        else if(random==l13.tag)
        {
            if([l13.text isEqualToString:zero])
            {
                l13.text=@"2";
                flag1=1;
                [self updaterow2];
            }
        }
        else if(random==l20.tag)
        {
            if([l20.text isEqualToString:zero])
            {
                l20.text=@"2";
                flag1=1;
                [self updaterow3];
            }
        }
        else if(random==l21.tag)
        {
            if([l21.text isEqualToString:zero])
            {
                l21.text=@"2";
                flag1=1;
                [self updaterow3];
            }
        }
        else if(random==l22.tag)
        {
            if([l22.text isEqualToString:zero])
            {
                l22.text=@"2";
                flag1=1;
                [self updaterow3];
            }
        }
        else if(random==l23.tag)
        {
            if([l23.text isEqualToString:zero])
            {
                l23.text=@"2";
                flag1=1;
                [self updaterow3];
            }
        }
        else if(random==l30.tag)
        {
            if([l30.text isEqualToString:zero])
            {
                l30.text=@"2";
                flag1=1;
                [self updaterow4];
            }
        }
        else if(random==l31.tag)
        {
            if([l31.text isEqualToString:zero])
            {
                l31.text=@"2";
                flag1=1;
                [self updaterow4];
            }
        }
        else if(random==l32.tag)
        {
            if([l32.text isEqualToString:zero])
            {
                l32.text=@"2";
                flag1=1;
                [self updaterow4];
            }
        }
        else
        {
            if([l33.text isEqualToString:zero])
            {
                l33.text=@"2";
                flag1=1;
                [self updaterow4];
            }
        }
    }
    
}

-(IBAction)left:(id)sender
{
    
    // code for 1st row
    flag=0;
    flag2=0;
    [self updaterow1];
    for(i=0;i<3;i++)
    {
        
    x:if(arr[0]==arr[1] && a!=0 && b!=0)
        {
            result=[@(a+b) stringValue];
            score1=[score.text integerValue];
            score1=score1+a+b;
            [self updatehighscore:score1];
            score.text=[@(score1) stringValue];
            l00.text=result;
            l01.text=l02.text;
            l02.text=l03.text;
            l03.text=@"0";
            flag=1;
            flag2=1;
            [self updaterow1];
        }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l00.text=result;
        l01.text=l03.text;
        l02.text=@"0";
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow1];
    }

        else if(a==0 && b!=0)
        {
            NSLog(@"a is 0- left button pressed");
            l00.text=l01.text;
            l01.text=l02.text;
            l02.text=l03.text;
            l03.text=@"0";
            flag2=1;
            [self updaterow1];
            goto x;
        }
        else{}
    y: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
        {
            result=[@(b+c) stringValue];
            score1=[score.text integerValue];
            score1=score1+b+c;
             [self updatehighscore:score1];
            score.text=[@(score1) stringValue];
            
            l00.text=result;
            l01.text=l03.text;
            l02.text=@"0";
            l03.text=@"0";
            flag=1;
            flag2=1;
            [self updaterow1];
        }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l01.text=result;
        l02.text=l03.text;
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow1];
    }
        else if(b==0 && c!=0)
        {
            NSLog(@"b is 0- left button pressed");
            l01.text=l02.text;
            l02.text=l03.text;
            l03.text=@"0";
            flag2=1;
            [self updaterow1];
            goto y;
        }
        else{}

    z:if(arr[2]==arr[3] && c!=0 && d!=0)
        {
            result=[@(c+d) stringValue];
            score1=[score.text integerValue];
            score1=score1+c+d;
             [self updatehighscore:score1];
            score.text=[@(score1) stringValue];
            
            l02.text=result;
            l03.text=@"0";
            flag=1;
            flag2=1;
            [self updaterow1];
        }
        else if(c==0 && d!=0)
        {
            NSLog(@"c is 0- left button pressed");
            l02.text=l03.text;
            l03.text=@"0";
            flag2=1;
            [self updaterow1];
            goto z;
        }
        else{}
        if(flag==1)
            break;

    }
   // code for 2nd row
    [self updaterow2];
    flag=0;
   
    for(j=0;j<3;j++)
    {
        
    x1:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l10.text=result;
        l11.text=l12.text;
        l12.text=l13.text;
        l13.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l10.text=result;
        l11.text=l13.text;
        l12.text=@"0";
        l13.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }

    else if(a==0 && b!=0)
    {
        NSLog(@"a is 0- left button pressed");
        l10.text=l11.text;
        l11.text=l12.text;
        l12.text=l13.text;
        l13.text=@"0";
        flag2=1;
        [self updaterow2];
        goto x1;
    }
    else{}
    y1: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l10.text=result;
        l11.text=l13.text;
        l12.text=@"0";
        l13.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l11.text=result;
        l12.text=l13.text;
        l13.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(b==0 && c!=0)
    {
        NSLog(@"b is 0- left button pressed");
        l11.text=l12.text;
        l12.text=l13.text;
        l13.text=@"0";
        flag2=1;
        [self updaterow2];
        goto y1;
    }
    else{}
        
    z1:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l12.text=result;
        l13.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(c==0 && d!=0)
    {
        NSLog(@"c is 0- left button pressed");
        l12.text=l13.text;
        l13.text=@"0";
        flag2=1;
        [self updaterow2];
        goto z1;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    // code for 3nd row
   
    [self updaterow3];
    flag=0;
    for(k=0;k<3;k++)
    {
        
    x11:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l20.text=result;
        l21.text=l22.text;
        l22.text=l23.text;
        l23.text=@"0";
        flag=1;
        [self updaterow3];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l20.text=result;
        l21.text=l23.text;
        l22.text=@"0";
        l23.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }
    else if(a==0 && b!=0)
    {
        l20.text=l21.text;
        l21.text=l22.text;
        l22.text=l23.text;
        l23.text=@"0";
        flag2=1;
        [self updaterow3];
        goto x11;
    }
    else{}
    y11: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l20.text=result;
        l21.text=l23.text;
        l22.text=@"0";
        l23.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l21.text=result;
        l22.text=l23.text;
        l23.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }

    else if(b==0 && c!=0)
    {
        l21.text=l22.text;
        l22.text=l23.text;
        l23.text=@"0";
        flag2=1;
        [self updaterow3];
        goto y11;
    }
    else{}
        
    z11:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l22.text=result;
        l23.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }
    else if(c==0 && d!=0)
    {
        l22.text=l23.text;
        l23.text=@"0";
        flag2=1;
        [self updaterow3];
        goto z11;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    // code for 4th row
    [self updaterow4];
    flag=0;
    for(l=0;l<3;l++)
    {
        
    x111:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l30.text=result;
        l31.text=l32.text;
        l32.text=l33.text;
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l30.text=result;
        l31.text=l33.text;
        l32.text=@"0";
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
    }
    else if(a==0 && b!=0)
    {
        NSLog(@"a is 0- left button pressed");
        l30.text=l31.text;
        l31.text=l32.text;
        l32.text=l33.text;
        l33.text=@"0";
        flag2=1;
        [self updaterow4];
        goto x111;
    }
    else{}
        
    y111: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l30.text=result;
        l31.text=l33.text;
        l32.text=@"0";
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
       // goto x111;
    }
        else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
        {
            result=[@(b+c) stringValue];
            score1=[score.text integerValue];
            score1=score1+b+c;
             [self updatehighscore:score1];
            score.text=[@(score1) stringValue];
            
            l31.text=result;
            l32.text=l33.text;
            l33.text=@"0";
            flag=1;
            flag2=1;
            [self updaterow4];
        }
    else if(b==0 && c!=0)
    {
        NSLog(@"b is 0- left button pressed");
        l31.text=l32.text;
        l32.text=l33.text;
        l33.text=@"0";
        flag2=1;
        [self updaterow4];
        goto y111;
    }
    else{}
        
    z111:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l32.text=result;
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
    }
    else if(c==0 && d!=0)
    {
        NSLog(@"c is 0- left button pressed");
        l32.text=l33.text;
        l33.text=@"0";
        flag2=1;
        [self updaterow4];
        goto z111;
    }
    else{}
        if(flag==1)
            break;
        
    }
    if(flag2==1)
    {
        [self randomnumbergenerater];
    }
    [self savehighscore];
}

-(IBAction)right:(id)sender
{
    // code for 1st row
    flag=0;
    flag2=0;
    [self updaterow1];
    for(i=0;i<3;i++)
    {
        
    x:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l03.text=result;
        l02.text=l01.text;
        l01.text=l00.text;
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow1];
    }
    else if(arr[3] == arr[1] && c==0 && d!=0 && b!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+d+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l03.text=result;
        l02.text=l00.text;
        
        l01.text=@"0";
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow1];
    }
    else if(d==0 && c!=0)
    {
        NSLog(@"d is 0-right button pressed");
        l03.text=l02.text;
        l02.text=l01.text;
        l01.text=l00.text;
        l00.text=@"0";
        flag2=1;
        [self updaterow1];
        goto x;
    }
    else{}
    y: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l03.text=result;
        l02.text=l00.text;
        l01.text=@"0";
        l00.text=@"0";
        flag=1;
        [self updaterow1];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l02.text=result;
        l01.text=l00.text;
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow1];
        
    }
    else if(c==0 && b!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l02.text=l01.text;
        l01.text=l00.text;
        l00.text=@"0";
        flag2=1;
        [self updaterow1];
        goto y;
    }
    else{}
        
    z:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l01.text=result;
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow1];
    }
    else if(b==0 && a!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l01.text=l00.text;
        l00.text=@"0";
        flag2=1;
        [self updaterow1];
        goto z;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    // code for 2nd row
    flag=0;
    [self updaterow2];
    for(j=0;j<3;j++)
    {
        
    x1:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l13.text=result;
        l12.text=l11.text;
        l11.text=l10.text;
        l10.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(arr[3] == arr[1] && c==0 && d!=0 && b!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l13.text=result;
        l12.text=l10.text;
        l11.text=@"0";
        l10.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(d==0 && c!=0)
    {
        NSLog(@"d is 0-right button pressed");
        l13.text=l12.text;
        l12.text=l11.text;
        l11.text=l10.text;
        l10.text=@"0";
        flag2=1;
        [self updaterow2];
        goto x1;
    }
    else{}
    y1: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l13.text=result;
        l12.text=l10.text;
        l11.text=@"0";
        l10.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
        //goto x111;
        
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l12.text=result;
        l11.text=l10.text;
        l10.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
        
    }

    else if(c==0 && b!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l12.text=l11.text;
        l11.text=l10.text;
        l10.text=@"0";
        flag2=1;
        [self updaterow2];
        goto y1;
    }
    else{}
        
    z1:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l11.text=result;
        l10.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow2];
    }
    else if(b==0 && a!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l11.text=l10.text;
        l10.text=@"0";
        flag2=1;
        [self updaterow2];
        goto z1;
    }
    else{}
        if(flag==1)
            break;
        
    }
     //code for 3rd row
    flag=0;
    [self updaterow3];
    for(k=0;k<3;k++)
    {
        
    x11:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l23.text=result;
        l22.text=l21.text;
        l21.text=l20.text;
        l20.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }
    else if(arr[3] == arr[1] && c==0 && d!=0 && b!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l23.text=result;
        l22.text=l20.text;
        l21.text=@"0";
        l20.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }
    else if(d==0 && c!=0)
    {
        NSLog(@"d is 0-right button pressed");
        l23.text=l22.text;
        l22.text=l21.text;
        l21.text=l20.text;
        l20.text=@"0";
        flag2=1;
        [self updaterow3];
        goto x11;
    }
    else{}
    y11: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        l23.text=result;
        l22.text=l20.text;
        l21.text=@"0";
        l20.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
        //goto x111;
        
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l22.text=result;
        l21.text=l20.text;
        l20.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
        
    }
    else if(c==0 && b!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l22.text=l21.text;
        l21.text=l20.text;
        l20.text=@"0";
        flag2=1;
        [self updaterow3];
        goto y11;
    }
    else{}
        
    z11:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l21.text=result;
        l20.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow3];
    }
    else if(b==0 && a!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l21.text=l20.text;
        l20.text=@"0";
        flag2=1;
        [self updaterow3];
        goto z11;
    }
    else{}
        if(flag==1)
            break;
        
    }
    //code for 4th row
    flag=0;
    [self updaterow4];
    for(l=0;l<3;l++)
    {
        
    x111:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l33.text=result;
        l32.text=l31.text;
        l31.text=l30.text;
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
    }
    else if(arr[3] == arr[1] && c==0 && d!=0 && b!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l33.text=result;
        l32.text=l30.text;
        l31.text=@"0";
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
    }
    else if(d==0 && c!=0 )
    {
        NSLog(@"d is 0-right button pressed");
        l33.text=l32.text;
        l32.text=l31.text;
        l31.text=l30.text;
        l30.text=@"0";
        flag2=1;
        [self updaterow4];
        goto x111;
    }
    else{}
    y111: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l33.text=result;
        l32.text=l30.text;
        l31.text=@"0";
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
        //goto x111;
     
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l32.text=result;
        l31.text=l30.text;
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
        
    }
    else if(c==0 && b!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l32.text=l31.text;
        l31.text=l30.text;
        l30.text=@"0";
        flag2=1;
        [self updaterow4];
        goto y111;
    }
    else{}
        
    z111:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l31.text=result;
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updaterow4];
    }
    else if(b==0 && a!=0)
    {
        NSLog(@"c is 0-right button pressed");
        l31.text=l30.text;
        l30.text=@"0";
        flag2=1;
        [self updaterow4];
        goto z111;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    if(flag2==1)
    {
        [self randomnumbergenerater];

    }
     [self savehighscore];
}



-(IBAction)up:(id)sender
{
    //code for 1st column
    flag=0;
    flag2=0;
    [self updatecolumn1];
    for(i=0;i<3;i++)
    {
        
    x:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l00.text=result;
        l10.text=l20.text;
        l20.text=l30.text;
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l00.text=result;
        l10.text=l30.text;
        l20.text=@"0";
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
        
    else if(a==0 && b!=0)
    {
        l00.text=l10.text;
        l10.text=l20.text;
        l20.text=l30.text;
        l30.text=@"0";
        flag2=1;
        [self updatecolumn1];
        goto x;
    }
    else{}
    y: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l00.text=result;
        l10.text=l30.text;
        l20.text=@"0";
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l10.text=result;
        l20.text=l30.text;
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(b==0 && c!=0)
    {
        l10.text=l20.text;
        l20.text=l30.text;
        l30.text=@"0";
        flag2=1;
        [self updatecolumn1];
        goto y;
    }
    else{}
        
    z:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l20.text=result;
        l30.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(c==0 && d!=0)
    {
        l20.text=l30.text;
        l30.text=@"0";
        flag2=1;
        [self updatecolumn1];
        goto z;
    }
    else{}
        if(flag==1)
            break;
        
    }
   
    //code for 2nd column
    flag=0;
    [self updatecolumn2];
    for(j=0;j<3;j++)
    {
        
    x1:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l01.text=result;
        l11.text=l21.text;
        l21.text=l31.text;
        l31.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l01.text=result;
        l11.text=l31.text;
        l21.text=@"0";
        l31.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
        
    else if(a==0 && b!=0)
    {
        l01.text=l11.text;
        l11.text=l21.text;
        l21.text=l31.text;
        l31.text=@"0";
        flag2=1;
        [self updatecolumn2];
        goto x1;
    }
    else{}
    y1: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l01.text=result;
        l11.text=l31.text;
        l21.text=@"0";
        l31.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l11.text=result;
        l21.text=l31.text;
        l31.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(b==0 && c!=0)
    {
        l11.text=l21.text;
        l21.text=l31.text;
        l31.text=@"0";
        flag2=1;
        [self updatecolumn2];
        goto y1;
    }
    else{}
        
    z1:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l21.text=result;
        l31.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(c==0 && d!=0)
    {
        l21.text=l31.text;
        l31.text=@"0";
        flag2=1;
        [self updatecolumn2];
        goto z1;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    //code for 3rd column
    flag=0;
    [self updatecolumn3];
    for(k=0;k<3;k++)
    {
        
    x11:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l02.text=result;
        l12.text=l22.text;
        l22.text=l32.text;
        l32.text=@"0";
        flag=1;
        [self updatecolumn3];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l02.text=result;
        l12.text=l32.text;
        l22.text=@"0";
        l32.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
        
    else if(a==0 && b!=0)
    {
        l02.text=l12.text;
        l12.text=l22.text;
        l22.text=l32.text;
        l32.text=@"0";
        flag2=1;
        [self updatecolumn3];
        goto x11;
    }
    else{}
    y11: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l02.text=result;
        l12.text=l32.text;
        l22.text=@"0";
        l32.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
        
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l12.text=result;
        l22.text=l32.text;
        l32.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
    else if(b==0 && c!=0)
    {
        l12.text=l22.text;
        l22.text=l32.text;
        l32.text=@"0";
        flag2=1;
        [self updatecolumn3];
        goto y11;
    }
    else{}
        
    z11:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l22.text=result;
        l32.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
    else if(c==0 && d!=0)
    {
        l22.text=l32.text;
        l32.text=@"0";
        flag2=1;
        [self updatecolumn3];
        goto z11;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    //code for 4th column
    flag=0;
    [self updatecolumn4];
    for(l=0;l<3;l++)
    {
        
    x111:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l03.text=result;
        l13.text=l23.text;
        l23.text=l33.text;
        l33.text=@"0";
        flag=1;
        [self updatecolumn4];
    }
    else if(arr[0]==arr[2] && b==0 && a!=0 && c!=0)
    {
        result=[@(a+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l03.text=result;
        l13.text=l33.text;
        l23.text=@"0";
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
        
    else if(a==0 && b!=0)
    {
        l03.text=l13.text;
        l13.text=l23.text;
        l23.text=l33.text;
        l33.text=@"0";
        flag2=1;
        [self updatecolumn4];
        goto x111;
    }
    else{}
    y111: if(arr[1]==arr[2] && b!=0 && c!=0 && a==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l03.text=result;
        l13.text=l33.text;
        l23.text=@"0";
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && a!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l13.text=result;
        l23.text=l33.text;
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(b==0 && c!=0)
    {
        l13.text=l23.text;
        l23.text=l33.text;
        l33.text=@"0";
        flag2=1;
        [self updatecolumn4];
        goto y111;
    }
    else{}
        
    z111:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l23.text=result;
        l33.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(c==0 && d!=0)
    {
        l23.text=l33.text;
        l33.text=@"0";
        flag2=1;
        [self updatecolumn4];
        goto z111;
    }
    else{}
        if(flag==1)
            break;
        
    }
    
    if(flag2==1)
    {
        [self randomnumbergenerater];
    }
     [self savehighscore];
}


-(IBAction)down:(id)sender
{
    // code for 1st column
    flag=0;
    flag2=0;
    [self updatecolumn1];
    for(i=0;i<3;i++)
    {
        
    x:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l30.text=result;
        l20.text=l10.text;
        l10.text=l00.text;
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(arr[1]==arr[3] && c==0 && b!=0 && d!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l30.text=result;
        l20.text=l00.text;
        l10.text=@"0";
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
        
    else if(d==0 && c!=0)
    {
        l30.text=l20.text;
        l20.text=l10.text;
        l10.text=l00.text;
        l00.text=@"0";
        flag2=1;
        [self updatecolumn1];
        goto x;
    }
    else{}
    y: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l30.text=result;
        l20.text=l00.text;
        l10.text=@"0";
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l20.text=result;
        l10.text=l00.text;
        l00.text=@"0";
        flag=1;
        flag2=1;
        
        [self updatecolumn1];
    }
    else if(c==0 && b!=0)
    {
        l20.text=l10.text;
        l10.text=l00.text;
        l00.text=@"0";
        flag2=1;
        [self updatecolumn1];
        goto y;
    }
    else{}
        
    z:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l10.text=result;
        l00.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn1];
    }
    else if(b==0 && a!=0)
    {
        l10.text=l00.text;
        l00.text=@"0";
        flag2=1;
        [self updatecolumn1];
        goto z;
    }
    else{}
        if(flag==1)
            break;
        
    }
     // code for 2nd column
    flag=0;
    [self updatecolumn2];
    for(j=0;j<3;j++)
    {
        
    x1:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l31.text=result;
        l21.text=l11.text;
        l11.text=l01.text;
        l01.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(arr[1]==arr[3] && c==0 && b!=0 && d!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l31.text=result;
        l21.text=l01.text;
        l11.text=@"0";
        l01.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
        
    else if(d==0 && c!=0)
    {
        l31.text=l21.text;
        l21.text=l11.text;
        l11.text=l01.text;
        l01.text=@"0";
        flag2=1;
        [self updatecolumn2];
        goto x1;
    }
    else{}
    y1: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l31.text=result;
        l21.text=l01.text;
        l11.text=@"0";
        l01.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l21.text=result;
        l11.text=l01.text;
        l01.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(c==0 && b!=0)
    {
        l21.text=l11.text;
        l11.text=l01.text;
        l01.text=@"0";
        flag2=1;
        [self updatecolumn2];
        goto y1;
    }
    else{}
        
    z1:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l11.text=result;
        l01.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn2];
    }
    else if(b==0 && a!=0)
    {
        l11.text=l01.text;
        l01.text=@"0";
        flag2=1;
        [self updatecolumn2];
        goto z1;
    }
    else{}
        if(flag==1)
            break;
        
    }
    //code for 3rd column
    flag=0;
    [self updatecolumn3];
    for(k=0;k<3;k++)
    {
        
    x11:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l32.text=result;
        l22.text=l12.text;
        l12.text=l02.text;
        l02.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
    else if(arr[1]==arr[3] && c==0 && b!=0 && d!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l32.text=result;
        l22.text=l02.text;
        l12.text=@"0";
        l02.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
        
    else if(d==0 && c!=0)
    {
        l32.text=l22.text;
        l22.text=l12.text;
        l12.text=l02.text;
        l02.text=@"0";
        flag2=1;
        [self updatecolumn3];
        goto x11;
    }
    else{}
    y11: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l32.text=result;
        l22.text=l02.text;
        l12.text=@"0";
        l02.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l22.text=result;
        l12.text=l02.text;
        l02.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn3];
    }
    else if(c==0 && b!=0)
    {
        l22.text=l12.text;
        l12.text=l02.text;
        l02.text=@"0";
        flag2=1;
        [self updatecolumn3];
        goto y11;
    }
    else{}
        
    z11:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l12.text=result;
        l02.text=@"0";
        flag=1;
        flag2=1;
        
        [self updatecolumn3];
    }
    else if(b==0 && a!=0)
    {
        l12.text=l02.text;
        l02.text=@"0";
        flag2=1;
        [self updatecolumn3];
        goto z11;
    }
    else{}
        if(flag==1)
            break;
        
    }
  //code for 4th column
    flag=0;
    [self updatecolumn4];
    for(l=0;l<3;l++)
    {
        
    x111:if(arr[2]==arr[3] && c!=0 && d!=0)
    {
        result=[@(c+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+c+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l33.text=result;
        l23.text=l13.text;
        l13.text=l03.text;
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(arr[1]==arr[3] && c==0 && b!=0 && d!=0)
    {
        result=[@(b+d) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+d;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l33.text=result;
        l23.text=l03.text;
        l13.text=@"0";
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
        
    else if(d==0 && c!=0)
    {
        l33.text=l23.text;
        l23.text=l13.text;
        l13.text=l03.text;
        l03.text=@"0";
        flag2=1;
        [self updatecolumn4];
        goto x111;
    }
    else{}
    y111: if(arr[1]==arr[2] && b!=0 && c!=0 && d==0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l33.text=result;
        l23.text=l03.text;
        l13.text=@"0";
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(arr[1]==arr[2] && b!=0 && c!=0 && d!=0)
    {
        result=[@(b+c) stringValue];
        score1=[score.text integerValue];
        score1=score1+b+c;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l23.text=result;
        l13.text=l03.text;
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(c==0 && b!=0)
    {
        l23.text=l13.text;
        l13.text=l03.text;
        l03.text=@"0";
        flag2=1;
        [self updatecolumn4];
        goto y111;
    }
    else{}
        
    z111:if(arr[0]==arr[1] && a!=0 && b!=0)
    {
        result=[@(a+b) stringValue];
        score1=[score.text integerValue];
        score1=score1+a+b;
         [self updatehighscore:score1];
        score.text=[@(score1) stringValue];
        
        l13.text=result;
        l03.text=@"0";
        flag=1;
        flag2=1;
        [self updatecolumn4];
    }
    else if(b==0 && a!=0)
    {
        l13.text=l03.text;
        l03.text=@"0";
        flag2=1;
        [self updatecolumn4];
        goto z111;
    }
    else{}
        if(flag==1)
            break;
        
    }
    if(flag2==1)
    {
        [self randomnumbergenerater];
    }
     [self savehighscore];
}



-(void)viewDidLoad
{
    l00.text=@"0";
    l01.text=@"0";
    l02.text=@"0";
    l03.text=@"0";
    l10.text=@"0";
    l11.text=@"0";
    l12.text=@"0";
    l13.text=@"0";
    l20.text=@"0";
    l21.text=@"2";
    l22.text=@"0";
    l23.text=@"0";
    l30.text=@"2";
    l31.text=@"0";
    l32.text=@"0";
    l33.text=@"0";
    score.text=@"0";
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *value=[defaults objectForKey:@"value"];
    highscore.text=value;

}
@end
